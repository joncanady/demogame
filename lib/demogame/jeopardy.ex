defmodule Demogame.Jeopardy do
  @moduledoc """
  Running a Jeopardy game!
  """

  import Ecto.Query, warn: false

  alias Demogame.{Game, Player, Repo, Question}

  def start_new_game do
    {:ok, game} = Repo.insert(%Game{
      code: generate_game_code(),
      type: "Jeopardy"
    })

    game
  end

  def update_game_status(game, new_status) do
    {:ok, game} = game
                  |> Game.changeset(%{status: new_status})
                  |> Repo.update
    game
  end

  def select_question_for_game(game, question) do
    {:ok, game} = game
                  |> Game.changeset(%{current_question_id: question.id})
                  |> Repo.update

    game
  end

  def unselect_question_for_game(game) do
    {:ok, game} = game
                  |> Game.changeset(%{current_question_id: nil})
                  |> Repo.update

    game
  end

  def find_game(id) do
    Repo.one from g in find_game_query(),
      where: g.id == ^id
  end

  def find_player(id) do
    Repo.get!(Player, id)
    |> Repo.preload(:game)
  end

  def find_game_by_code(code) do
    Repo.one from g in find_game_query(),
      where: g.code == ^code
  end

  def find_question(id), do: Repo.get!(Question, id)
  def find_question(category_id, points) do
    Repo.one from q in Question,
      where: q.category_id == ^category_id,
      where: q.points == ^points
  end

  def players_for_game(game_id) do
    Repo.all(
      from p in Player,
        where: p.game_id == ^game_id
    )
  end

  def player_for_game_with_name(game_id, name) do
    Repo.get_by(Player, game_id: game_id, name: name)
  end

  def evaluate_answer(question, answer_id) do
    correct = answer_id == "1"
    if correct do
      unselect_question_for_game(question.game)
    end

    correct
  end

  def add_player_to_game(nil, _name) do
    {:error, "Game not found."}
  end

  def add_player_to_game(%Game{status: "waiting_for_players"} = game, player_name) do
    if player = player_for_game_with_name(game.id, player_name) do
      {:ok, player}
    else
      %Player{}
      |> Player.changeset(%{game_id: game.id, name: player_name})
      |> Repo.insert()
    end
  end

  # TODO: Cache with nebulex?
  def add_player_to_game(%Game{} = game, player_name) do
    if player = player_for_game_with_name(game.id, player_name) do
      {:rejoin, player}
    else
      {:error, "Game is in progress and cannot be joined."}
    end
  end

  # TODO: Cache with nebulex?
  def set_controlling_player(player) do
    game = find_game(player.game_id)
    {:ok, _game} = game
                   |> Game.changeset(%{controlling_player_id: player.id})
                   |> Repo.update

    player
  end

  def get_controlling_player(%Game{controlling_player_id: nil}), do: nil
  def get_controlling_player(%Game{} = game) do
    Repo.one(
      from p in Player,
      where: p.id == ^game.controlling_player_id
    )
  end

  def get_or_assign_controlling_player(%Game{controlling_player_id: nil} = game) do
    game = Repo.preload(game, :players)
    random_player = Enum.random(game.players)
    set_controlling_player(random_player)

    random_player
  end

  def get_or_assign_controlling_player(%Game{controlling_player: controlling_player}) do
    controlling_player
  end

  defp generate_game_code() do
    random_string(4) |> String.upcase()
  end

  defp random_string(length) do
    :crypto.strong_rand_bytes(length) |> Base.url_encode64() |> binary_part(0, length)
  end

  defp find_game_query do
    from g in Game,
      left_join: p in assoc(g, :players),
      left_join: cont in assoc(g, :controlling_player),
      preload: [players: p, controlling_player: cont]
  end
end
