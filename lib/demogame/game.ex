defmodule Demogame.Game do
  use Ecto.Schema
  import Ecto.Changeset

  @default_status "waiting_for_players"

  schema "games" do
    field :code, :string
    field :type, :string, default: "jeopardy"
    field :status, :string, default: @default_status
    field :current_question_id, :integer
    belongs_to :controlling_player, Demogame.Player
    has_many :players, Demogame.Player

    timestamps()
  end

  def changeset(game, attrs) do
    game
    |> cast(attrs, [:status, :controlling_player_id, :current_question_id])
    |> validate_required([:status])
  end
end
