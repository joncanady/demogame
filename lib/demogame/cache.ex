defmodule Demogame.Cache do
  use Nebulex.Cache,
    otp_app: :demogame,
    adapter: Nebulex.Adapters.Local
end
