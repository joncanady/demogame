defmodule DemogameWeb.JeopardyLive.Join do
  use DemogameWeb, :live_view

  alias Demogame.Jeopardy

  @impl true
  def mount(_params, _session, socket) do
    {:ok, assign(socket, joined: false, game_id: nil, name: nil)}
  end

  @impl true
  def handle_params(%{}, _url, socket) do
    {:noreply,
     socket
     |> assign(:page_title, "Join Game")}
  end

  @impl true
  def handle_event("join", %{"game_code" => code, "name" => name}, socket) do
    game = Jeopardy.find_game_by_code(code)

    case Jeopardy.add_player_to_game(game, name) do
      {:ok, player} ->
        broadcast_join(game.id)
        Phoenix.PubSub.subscribe(Demogame.PubSub, "game:#{game.id}")
        {:noreply, assign(socket, %{joined: true, game_id: game.id, player: player})}
      {:rejoin, player} ->
        {:noreply,
         socket
         |> push_redirect(to: Routes.play_path(socket, DemogameWeb.JeopardyLive.Play, player.id))}
      {:error, msg} ->
        {:noreply,
         socket
         |> put_flash(:error, msg)}
    end
  end

  @impl true
  def handle_event("start_game", _params, socket) do
    game = Jeopardy.find_game(socket.assigns.game_id)

    broadcast_start(game.id)
    {:noreply, socket}
  end

  @impl true
  def handle_info(:start_button_pressed, socket) do
    player = socket.assigns.player

    {:noreply,
      socket
      |> push_redirect(to: Routes.play_path(socket, DemogameWeb.JeopardyLive.Play, player.id))}
  end

  @impl true
  def handle_info(_msg, socket) do
    {:noreply, socket}
  end

  defp broadcast_join(game_id) do
    Phoenix.PubSub.broadcast(
      Demogame.PubSub,
      "game:#{game_id}",
      :player_join
    )
  end

  defp broadcast_start(game_id) do
    Phoenix.PubSub.broadcast(
      Demogame.PubSub,
      "game:#{game_id}",
      :start_button_pressed
    )
  end
end
