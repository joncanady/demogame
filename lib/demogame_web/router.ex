defmodule DemogameWeb.Router do
  use DemogameWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {DemogameWeb.LayoutView, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", DemogameWeb do
    pipe_through :browser

    live "/", JeopardyLive.Title
    live "/lobby/:id", JeopardyLive.Lobby, as: "lobby"
    live "/board/:id", JeopardyLive.Board, as: "board"
    live "/join", JeopardyLive.Join, :join, as: "join"
    live "/play/:id", JeopardyLive.Play, as: "play"
    live "/select/:id", JeopardyLive.SelectCategory, as: "select"
    live "/answer/:question_id/:player_id", JeopardyLive.Answer, as: "answer"
    live "/question/:game_id/:id", JeopardyLive.Question, as: "question"
  end

  # Other scopes may use custom stacks.
  # scope "/api", DemogameWeb do
  #   pipe_through :api
  # end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through :browser
      live_dashboard "/dashboard", metrics: DemogameWeb.Telemetry
    end
  end
end
