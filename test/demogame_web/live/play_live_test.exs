defmodule Demogame.PlayLiveTest do
  use DemogameWeb.ConnCase

  import Phoenix.LiveViewTest
  alias Demogame.Jeopardy

  test "The player who controls the board can select a category", %{conn: conn} do
    game = insert(:running_game)
    player = insert(:player, game: game)

    Jeopardy.set_controlling_player(player)

    {:error, {:live_redirect, %{to: to}}} = live(conn, "/play/#{player.id}")
    assert(to == "/select/#{player.id}")
  end
end
