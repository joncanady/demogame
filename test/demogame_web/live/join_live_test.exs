defmodule DemogameWeb.JoinLiveTest do
  use DemogameWeb.ConnCase

  import Phoenix.LiveViewTest

  test "when a player joins an invalid game", %{conn: conn} do
    {:ok, player_view, _disconnected_html} = live(conn, "/join")

    assert player_view
           |> form("#join", %{name: "Player Name", game_code: "AAAA"})
           |> render_submit() =~ "Game not found."
  end

  test "when a player joins an existing game", %{conn: conn} do
    {:ok, player_view, _disconnected_html} = live(conn, "/join")
    game = insert(:game, %{status: "waiting_for_players"})

    assert player_view
           |> form("#join", %{name: "Player Name", game_code: game.code})
           |> render_submit() =~ "all set"
  end

  test "when a player joins a game that is not waiting for players", %{conn: conn} do
    {:ok, player_view, _disconnected_html} = live(conn, "/join")
    game = insert(:game, %{status: "running"})

    assert player_view
           |> form("#join", %{name: "Player Name", game_code: game.code})
           |> render_submit() =~ "Game is in progress and cannot be joined."
  end

  test "when a player re-joins an active game", %{conn: conn} do
    {:ok, player_view, _disconnected_html} = live(conn, "/join")
    game   = insert(:game, %{status: "running"})
    player = insert(:player, game: game)

    assert player_view
    |> form("#join", %{name: player.name, game_code: game.code})
    |> render_submit()

    assert_redirect(player_view, "/play/#{player.id}")
  end

  test "when a player presses the 'Start Game' button", %{conn: conn} do
    {:ok, player_view, _disconnected_html} = live(conn, "/join")
    game = insert(:game, %{status: "waiting_for_players"})
    player = insert(:player, game: game)

    player_view
    |> form("#join", %{name: player.name, game_code: game.code})
    |> render_submit()

    player_view
    |> render_click(:start_game)

    assert_redirect(player_view, "/play/#{player.id}")
  end
end
