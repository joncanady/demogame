# Demogame

An unfortunately-named demo project.

## Branch Name

The default branch for this repo is `main`.

## Rules

Game has 2+ players and 1 host.

* ok: Start the Jeopardy game.
* ok: Contestants join using a 4-character code and their name.
* ok: Randomly select a player to control the board -- select category and value.
* The player view acts as a "buzzer" -- press it to signal you want to answer the question.
  * Host has a view where they can see all player names, who buzzed in, and the correct answer.
  * -- maybe automated host so the game acts as host?
* Player views switch to "PLAYER is answering the question" or "Select answer"
* If the player answers correctly, they get the points/money and get to choose the next category.
* If the player gets the question wrong, they cannot buzz in again and other players mayh buzz in.
* Players have 5 seconds to answer the question before their time is out.
* Players who's time has run out cannot attempt to answer the question again.
* Questions also have a five second time limit for someone to buzz in.
    * If no player buzzes in, the original player gets to select a new category.
* Main screen shows the game board and the questions.

## Development TODOs

Got the "answered the question wrong" flow mostly right. There's a refresh issue
with Process.send_after so maybe we'll write a GenServer or something to keep
track of state.
# TODO: Handle this in the question.ex liveview and do not reassign board control.
(answer.ex)

* TODO: Update game state to indicate selected question while one is being answered.
* After a question has been selected, mark it as unavailable somehow.

* Ensure a second player can't take over the first player's session Trivia.add_player_to_game(socket.assigns.game.id, name)
* Stop using the SQL database as a frontend -- use nebulex to cache values, then write-through.

### Maybe?

* factor out random_game_code into a general module for use in multiple games
* Enure that we can re-use codes for games, and ensure no two games can use the same.
* Scope the Jeopardy routes to `/jeopardy`

## Deployment

Ready to run in production? Please [check our deployment guides](https://hexdocs.pm/phoenix/deployment.html).
