defmodule Demogame.Repo.Migrations.AddTimestampsToPlayer do
  use Ecto.Migration

  def change do
    alter table(:players) do
      timestamps()
    end
  end
end
