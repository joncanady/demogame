defmodule Demogame.Repo.Migrations.AddCurrentlySelectedQuestionToGame do
  use Ecto.Migration

  def change do
    alter table(:games) do
      add :current_question_id, :integer
    end
  end
end
