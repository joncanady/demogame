defmodule Demogame.Repo.Migrations.AddControllingToPlayers do
  use Ecto.Migration

  def change do
    alter table(:players) do
      add :controlling, :boolean
    end
  end
end
